﻿Shader "Holistic/AllProps"
{
    Properties
    {
        _myColor ("Example Color", Color) = (1, 1, 1, 1)
        _myRange ("Example Range", Range(0, 5)) = 1
        _myTex ("Example Texture", 2D) = "white" {}
        _myCube ("Example Cube", CUBE) = "" {}
        _myFloat ("Example Float", Float) = 0.5
        _myVector ("Example Vector", Vector) = (0.5, 1, 1, 1)

        _myDiffuse ("Diffuse Texture", 2D) = "white" {}
        _myEmissive ("Emission Texture", 2D) = "black" {}
    }
    SubShader
    {
        CGPROGRAM
        #pragma surface surf Lambert

        fixed4 _myColor;
        half _myRange;
        sampler2D _myTex;
        samplerCUBE _myCube;
        float _myFloat;
        float4 _myVector;
        
        sampler2D _myDiffuse;
        sampler2D _myEmissive;

        struct Input
        {
            float2 uv_myTex;
            float3 worldRefl;
        };

        void surf (Input IN, inout SurfaceOutput o)
        {
            o.Albedo = tex2D(_myDiffuse, IN.uv_myTex).rgb;
            o.Emission = tex2D(_myEmissive, IN.uv_myTex).rgb;
        }
        
        ENDCG
    }
    FallBack "Diffuse"
}
