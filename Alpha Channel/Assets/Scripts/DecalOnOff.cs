﻿using UnityEngine;

[ExecuteInEditMode]
public class DecalOnOff : MonoBehaviour
{
    private Material _material;
    private bool _showDecal = false;

    private void OnMouseDown()
    {
        _showDecal = !_showDecal;
        _material.SetFloat("_ShowDecal", _showDecal ? 1 : 0);
    }

    private void Start()
    {
        _material = this.GetComponent<Renderer>().sharedMaterial;
    }
}
