﻿Shader "Holistic/ScrollingWave"
{
    Properties
    {
        _MainTex("Diffuse", 2D) = "white" {}
        _Tint("Colour Tint", Color) = (1, 1, 1, 1)
        _WaveFreq("Wave Frequency", Range(0, 5)) = 3
        _WaveSpeed("Wave Speed", Range(0, 100)) = 10
        _WaveAmp("Wave Amplitude", Range(0, 1)) = 0.5
        _ScrollX ("Scroll X", Range(-5, 5)) = 1
        _ScrollY ("Scroll Y", Range(-5, 5)) = 1
    }
    SubShader
    {
        CGPROGRAM
        #pragma surface surf Lambert vertex:vert

        struct Input
        {
            float2 uv_MainTex;
            float3 vertColor;
        };

        sampler2D _MainTex;
        float4 _Tint;
        float _WaveFreq;
        float _WaveSpeed;
        float _WaveAmp;
        float _ScrollX;
        float _ScrollY;

        struct appdata
        {
            float4 vertex: POSITION;
            float3 normal: NORMAL;
            float4 texcoord: TEXCOORD0;
            float4 texcoord1: TEXCOORD1;
            float4 texcoord2: TEXCOORD2;
        };

        void vert (inout appdata v, out Input o)
        {
            UNITY_INITIALIZE_OUTPUT(Input, o);
            float t = _Time * _WaveSpeed;
            float waveHeight = sin(t + v.vertex.x * _WaveFreq) + _WaveAmp;
            v.vertex.y += waveHeight;
            v.normal = normalize(float3(v.normal.x + waveHeight, v.normal.y, v.normal.z));
            o.vertColor = waveHeight + 2;
        }

        void surf(Input IN, inout SurfaceOutput o)
        {
            _ScrollX *= _Time;
            _ScrollY *= _Time;
            float2 newuv = IN.uv_MainTex + float2(_ScrollX, _ScrollY);
            float4 c = tex2D(_MainTex, newuv);
            o.Albedo = c * IN.vertColor.rgb;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
